#
# Be sure to run `pod lib lint SourceryAdditions.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'SourceryAdditions'
  s.version          = '0.1.1'
  s.summary          = 'Sourcery templates'

  s.description      = <<-DESC
Templates for Sourcery code gen framework.
                       DESC

  s.homepage         = 'https://bitbucket.org/MikhailMulyar/sourceryadditions'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'mikhailmulyar' => 'mulyarm@gmail.com' }
  s.source           = { :git => 'https://bitbucket.org/MikhailMulyar/sourceryadditions.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = 'SourceryAdditions/Classes/**/*'
  s.preserve_paths = 'SourceryAdditions/**/*'

  # s.resource_bundles = {
  #   'SourceryAdditions' => ['SourceryAdditions/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
