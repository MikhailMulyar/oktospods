#
# Be sure to run `pod lib lint OktosWalletKit.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'OktosWalletKit'
  s.version          = '0.2.1'
  s.summary          = 'OktosWalletKit is cryptocurrency wallets library for Oktos project'

  s.description      = <<-DESC
  OktosWalletKit is cryptocurrency wallets library for Oktos project.
                       DESC

  s.homepage         = 'https://bitbucket.org/MikhailMulyar/oktoswalletkit'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'mikhailmulyar' => 'mulyarm@gmail.com' }
  s.source           = { :git => 'https://bitbucket.org/MikhailMulyar/oktoswalletkit.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'
#  s.static_framework = true
  s.source_files = 'OktosWalletKit/Classes/**/*'

  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '4' }

  s.module_name = 'OktosWalletKit'
  s.dependency 'CKMnemonic'
  s.dependency 'Sodium'
  s.framework = 'AVFoundation'
  s.vendored_frameworks = 'OktosWalletKit/Frameworks/ethers.framework' , 'OktosWalletKit/Frameworks/CoreBitcoin.framework'
#  s.module_map = 'OktosWalletKit/OktosWalletKit.modulemap'
#  s.user_target_xcconfig = { 'FRAMEWORK_SEARCH_PATHS' => '"${PODS_ROOT}/../../../OktosWalletKit/Classes/Ethers"' }
#  s.xcconfig = { 'SWIFT_INCLUDE_PATHS' => '$(SRCROOT)/../../OktosWalletKit/Classes/Ethers/ethers/Headers/*.h' }
end
